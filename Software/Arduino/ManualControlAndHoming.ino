/*
   TODO:: Update the arduino program to Make the user able to choose the motor driver micro stepping mode. find and equation that helps to automatically adjust the the "steps" variable value.
   TODO:: update the mobile app to ask the user on the beginning only about the [homing position(done), microstepping mode].
   TODO:: Update the arduino program to make the code only iterates around the "homing position" & "microstepping mode" only once on the void setup() function.

   DATA::left arrow button sends --> 1.
   DATA::right arrow button sends --> 2.
   DATA::stop button sends --> 3.

   DATA::rail length (1cm - 1000cm) --> (201 - 1200).

  DATA:: motor acceleration spinner Very High --> 14.
   DATA:: motor acceleration spinner High --> 11
   DATA:: motor acceleration spinner Medium --> 12
   DATA:: motor acceleration spinner Low --> 13
   DATA:: motor acceleration spinner Very Low --> 15

   DATA:: motor speed slider (1 step/sec. - 4000 step/sec.) --> (5001 - 9000).

   DATA::delay start checkbox is true --> 7.
   DATA::delay start checkbox is false --> 8.

   DATA::left end homing --> 16.
   DATA::right end homing --> 17.

   DATA::Smooth movement Type --> 18.
   DATA::Very Smooth movement Type --> 19.

   1301 --> 2300
*/

#include <MultiStepper.h>
#include <AccelStepper.h>

#define stepPin 2
#define dirPin 3

bool homingPositionFlag = false;
int startupSetupFlag = 0;
bool delayedStart = false;

int incomingData = 0;
int movementDistance = 50;
long steps = 0;       //50cm rail by default @1/8 microstepping.
int microStepResolution = 0;    //4  or  16
long railLength = 0;
int sliderSpeed = 10;

AccelStepper stepper(AccelStepper::DRIVER, stepPin, dirPin);       //create an object. the pin "2" is the step pin, "3" is the direction pin.

void setup() {
  pinMode(stepPin, OUTPUT);
  pinMode(dirPin, OUTPUT);
  Serial.begin(9600);

  stepper.setMaxSpeed(10.00);        //The fastest motor speed that can be reliably supported is about 4000 steps per second at a clock frequency of 16 MHz on Arduino such as Uno
  stepper.setAcceleration(500.00);     //1600 (40%) (Medium Acceleration rate)

  while (startupSetupFlag < 3) {
    if (Serial.available() > 1) {
      unsigned int dataOne = Serial.read();
      unsigned int dataOne1 = Serial.read();
      unsigned int incomingData = (dataOne1 * 256) + dataOne;

      //**************************************************************Motor Homing Part**************************************************
      if (incomingData == 16) {      //left end homing position.
        stepper.setCurrentPosition(steps);
        homingPositionFlag = false;
        startupSetupFlag++;
      } else if (incomingData == 17) {    //right end homing position.
        stepper.setCurrentPosition(-(steps));
        homingPositionFlag = true;
        startupSetupFlag++;
      }
      //**************************************************************microstep resolution Part**************************************************
      if (incomingData == 18) {
        microStepResolution = 4;                   //50cm rail length @1/4 microstep resolution.
        startupSetupFlag++;
      } else if (incomingData == 19) {
        microStepResolution = 16;                  //50cm rail length @1/16 microstep resolution.
        startupSetupFlag++;
      }

      if (incomingData >= 1301 && incomingData <= 2300) {
        railLength = incomingData - 1300; //from raw data to cm.
        if (microStepResolution == 4) {
          steps = ((6100L * railLength) / 50L);
          startupSetupFlag++;
        }
        else if (microStepResolution == 16) {
          steps = ((25000L * railLength) / 50L);
          startupSetupFlag++;
        }
      }
    }
    //Serial.println(startupSetupFlag);
  }
  /*
   * *********** *********** **********For Debugging Purposes* *********** *********** **********
    Serial.print("rail length: ");
    Serial.print(railLength);
    Serial.print("    number of steps:  ");
    Serial.print(steps);
    Serial.print("    Homing position: ");
    Serial.print(stepper.currentPosition());
    Serial.print("    microstep resolution: ");
    Serial.println(microStepResolution);*/
}

void loop() {

  if (Serial.available() > 1) {

    unsigned int dataOne = Serial.read();
    unsigned int dataOne1 = Serial.read();
    unsigned int incomingData = (dataOne1 * 256) + dataOne;
    //Serial.print("raw data:     ");
    //Serial.println(incomingData);

    //**************************************************************Motor Control Part**************************************************
    if (incomingData == 1 && stepper.isRunning() == false && stepper.currentPosition() != 6050 && homingPositionFlag == true) {
      if (delayedStart == true) { //use millis to delay 15 seconds.
        delay(15000);     //wait 15 seconds.
      }
      stepper.setCurrentPosition(0);
      stepper.moveTo(steps);             //from end to end (@ 1/4 step).
      homingPositionFlag = false;

      /*Serial.print("rail length: ");
        Serial.print(railLength);
        Serial.print("    number of steps:  ");
        Serial.print(steps);
        Serial.print("    Homing position: ");
        Serial.print(stepper.currentPosition());
        Serial.print("    microstep resolution: ");
        Serial.println(microStepResolution);*/
    }

    else if (incomingData == 2 && stepper.isRunning() == false && stepper.currentPosition() != -6050 && homingPositionFlag == false) {
      if (delayedStart == true) { //use millis to delay 15 seconds.
        delay(15000);     //wait 15 seconds.
      }
      stepper.setCurrentPosition(0);
      stepper.moveTo(-(steps));             //from end to end (@ 1/4 step).
      homingPositionFlag = true;

      /*Serial.print("rail length: ");
        Serial.print(railLength);
        Serial.print("    number of steps:  ");
        Serial.print(steps);
        Serial.print("    Homing position: ");
        Serial.print(stepper.currentPosition());
        Serial.print("    microstep resolution: ");
        Serial.println(microStepResolution);*/
    }

    else if (incomingData == 3 && stepper.isRunning() == true) {
      homing();
    }

    //**************************************************************Set Max. Speed Part**************************************************
    else if (incomingData >= 5001 && incomingData <= 9000) {
      sliderSpeed = incomingData - 5000;
      stepper.setMaxSpeed(sliderSpeed);
    }

    //**************************************************************Set Delayed Start Part**************************************************
    else if (incomingData == 7) {   //delayed start (15 seconds) is checked "true"
      delayedStart = true;
    }
    else if (incomingData == 8) {   //delayed start (15 seconds) is not checked "false"
      delayedStart = false;
    }

    //**************************************************************Set movement distance Part**************************************************
    else if (incomingData >= 201 && incomingData <= 1200) {  //convertin from rail length into number of steps. (upto 10 meters)
      movementDistance = incomingData - 200; //from raw data to cm.
      if (microStepResolution == 4) {
        steps = ((6100L * movementDistance) / 50L);
      }
      else if (microStepResolution == 16) {
        steps = ((25000L * movementDistance) / 50L);
      }
      /*Serial.print("rail length:  ");
        Serial.print(movementDistance);
        Serial.print("      number of steps:  ");
        Serial.println(steps);*/
    }

    //**************************************************************Set Acceleration Part**************************************************
    else if (incomingData == 11 && stepper.isRunning() == false) {          //HIGH
      stepper.setAcceleration(3000);
    }

    else if (incomingData == 12 && stepper.isRunning() == false) {          //Medium
      stepper.setAcceleration(1000);
    }

    else if (incomingData == 13 && stepper.isRunning() == false) {          //Low
      stepper.setAcceleration(500);
    }

    else if (incomingData == 14 && stepper.isRunning() == false) {          //Very High
      stepper.setAcceleration(4000);
    }

    else if (incomingData == 15 && stepper.isRunning() == false) {          //Very Low
      stepper.setAcceleration(10);
    }
  }
  stepper.run();
}

void homing() {
  if (stepper.currentPosition() > 0) {
    homingPositionFlag = true;
  } else {
    homingPositionFlag = false;
  }
  stepper.moveTo(0);
}
