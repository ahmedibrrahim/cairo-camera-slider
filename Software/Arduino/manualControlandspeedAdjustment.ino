/*
   TODO::build a manual homing.

   DATA::left arrow button sends "1".
   DATA::right arrow button sends "2".
   DATA::homing button sends "3".
*/

#include <MultiStepper.h>
#include <AccelStepper.h>

int incomingData = 0;
bool motorFlag = false;

#define stepPin 2
#define dirPin 3

void setup() {
  pinMode(stepPin, OUTPUT);
  pinMode(dirPin, OUTPUT);
  pinMode(13, OUTPUT);
  Serial.begin(9600);
}

void loop() {
  int stepperCurrentPosition = 0;
  while (Serial.available() > 0) {
    incomingData = Serial.read();
    //Serial.print("the Incoming Data is:     ");
    //Serial.println(incomingData);

    if (incomingData == 1) {
      motorFlag = true;
      while (motorFlag == true) {

        digitalWrite(dirPin, HIGH);

        digitalWrite(stepPin, HIGH);
        delayMicroseconds(500);
        digitalWrite(stepPin, LOW);
        delayMicroseconds(500);

        incomingData = Serial.read();
        if (incomingData == 3) {
          motorFlag = false;
        }
      }
    }
    if (incomingData == 2) {
      motorFlag = true;
      while (motorFlag == true) {
        digitalWrite(dirPin, LOW);

        digitalWrite(stepPin, HIGH);
        delayMicroseconds(500);
        digitalWrite(stepPin, LOW);
        delayMicroseconds(500);

        incomingData = Serial.read();
        if (incomingData == 3) {
          motorFlag = false;
        }
      }
    }

    if (incomingData == 3) {
      motorFlag = false;
      Serial.println("false - stop!");
    }
  }
}
