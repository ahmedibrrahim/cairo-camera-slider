For someone who loves to shoot some random hobbyist videos, it’s somehow expensive to buy a motorized camera slider. So, I built my own. In this tutorial, we will go through each step to build your own Bluetooth controlled motorized camera slider.

this project is under beerware license.
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <phk@FreeBSD.ORG> wrote this file.  As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return.   Poul-Henning Kamp
 * ----------------------------------------------------------------------------

 Project step-by-step tutorial https://makesomestuff.org/arduino-motorized-camera-slider/
